<?php 
// Catch cURL/Wget requests
//if (isset($_POST['envio']) && !empty($_POST['envio']) || isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/^(curl|wget)/i', $_SERVER['HTTP_USER_AGENT'])) {
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' || 
    isset($_SERVER['HTTP_USER_AGENT']) && 
    preg_match('/^(curl|wget)/i', $_SERVER['HTTP_USER_AGENT'])) {


    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $tamanho = (isset($_POST['tamanho'])) ? (int)$_POST['tamanho'] : 10;
        $maiusculas = ( isset($_POST['maiusculas']) ) ? $_POST['maiusculas'] : false;
        $minusculas = ( isset($_POST['minusculas']) ) ? $_POST['minusculas'] : false;
        $numeros = ( isset($_POST['numeros']) ) ? $_POST['numeros'] : false;
        $simbolos = ( isset($_POST['simbolos']) ) ? $_POST['simbolos'] : false;
    } else {
        $tamanho = 6;
        $maiusculas = true;
        $minusculas = true;
        $numeros = true;
        $simbolos = true;
    }

    $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiúsculas
    $mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
    $nu = "0123456789"; // $nu contem os números
    $si = "!@#$%&*()_+="; // $si contem os símbolos

    if ($maiusculas) { $senha .= str_shuffle($ma); }

    if ($minusculas) { $senha .= str_shuffle($mi); }

    if ($numeros){ $senha .= str_shuffle($nu); }

    if ($simbolos) { $senha .= str_shuffle($si); }

    $senha = utf8_decode($senha);

    echo substr(str_shuffle($senha),0,$tamanho);
    exit();
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gerador de Senhas</title>

        <meta name="description" content="Gerador de Senhas">
        <meta name="keywords" content="Gerador de Senhas, Gerador de Senha, Senha, Senhas">
        <meta name="author" content="Lucas Saliés Brum">

        <meta property="og:title" content="Gerador de Senhas" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://senha.lucasbrum.net" />
        <meta property="og:image" content="https://senha.lucasbrum.net/img/favicon.png" />

        <link rel="stylesheet" href="css/bulma.min.css">
        <link rel="stylesheet" href="css/fontawesome-5.11.2.min.css">
        <link rel="stylesheet" href="css/bulma-switch.min.css">
        <link rel="stylesheet" href="css/principal.css">

        <link rel="shortcut icon" href="img/favicon.ico">
    </head>
    <body>

        <section class="hero is-danger is-fullheight">
            <div class="hero-body">

                <div class="container">

                    <div class="columns is-centered">
                        <div class="column is-half">

                            <div class="tile is-ancestor">
                                <div class="tile is-parent">
                                    <article class="tile is-child">
                                        <p class="title">Gerador de Senhas</p>
                                        <p class="subtitle">Crie uma senha segura</p>
                                        <form id="formulario" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                            <div class="field">
                                                <div class="control">
                                                    <label class="label">Caracteres</label>
                                                    <div class="select is-small">
                                                        <select name="tamanho">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10" selected>10</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="field checkArray">
                                                <label class="label">Opções</label>
                                                <div class="control">

                                                    <div class="field">
                                                        <input id="minusculas" type="checkbox" name="minusculas" class="checks switch is-info" checked="checked">
                                                        <label for="minusculas">Minúsculas</label>

                                                        <input id="maiusculas" type="checkbox" name="maiusculas" class="checks switch is-success" checked="checked">
                                                        <label for="maiusculas">Maiúsculas</label>

                                                        <input id="numeros" type="checkbox" name="numeros" class="checks switch is-warning" checked="checked">
                                                        <label for="numeros">Números</label>

                                                        <input id="simbolos" type="checkbox" name="simbolos" class="checks switch is-info" checked="checked">
                                                        <label for="simbolos">Símbolos</label>
                                                    </div>
                                                </div>
                                            </div>  

                                            <div class="field has-addons">
                                                <div class="control">
                                                    <a class="button copiar" data-clipboard-target="#senha">
                                                        <span class="icon is-small">
                                                            <i class="fas fa-copy"></i>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="control">
                                                    <input id="senha" class="input" type="text" placeholder="Senha">
                                                </div>
                                                <div class="control">
                                                    <input class="button is-success" type="submit" name="envio" value="Gerar" />
                                                </div>
                                            </div>
                                        </form>
                                        <br />
                                        <p class="is-size-7 is-italic">
                                            Feito por <a href="https://lucasbrum.net">Lucas Saliés Brum</a>, código fonte no <a href="https://gitlab.com/sistematico/gerador-de-senhas">GitLab</a>.
                                        </p>
                                    </article>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </section>            

        <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="js/clipboard.min.js"></script>
        <script src="js/script.js"></script>
    </body>
</html>