$(function() {

    //$("#numeros").prop("checked", false);

    $('select#tamanho>option:eq(3)').attr('selected', true);

    if(localStorage.getItem('senha')) {
        $("#senha").val(localStorage.getItem('senha'));    
    }

    //if(localStorage.getItem('tamanho')) {
    //    let tam = localStorage.getItem('tamanho');
    //    $('select#tamanho>option:eq(' + tam +')').attr('selected', true);
    //} 

    if(localStorage.getItem('minusculas')) {
        $('input[name="minusculas"]').prop("checked", localStorage.getItem('minusculas'));
    } 

    if(localStorage.getItem('maiusculas')) {
        $('input[name="maiusculas"]').prop("checked", localStorage.getItem('maiusculas'));
        //$("#maiusculas").prop("checked", localStorage.getItem('maiusculas') )
    } 

    if(localStorage.getItem('numeros')) {
        $("#numeros").prop("checked", localStorage.getItem('numeros') )
    } 

    if(localStorage.getItem('simbolos')) {
        $("#simbolos").prop( "checked", localStorage.getItem('simbolos') )
    } 

    var clipboard = new ClipboardJS('.copiar');

    function recupera() {
        $.ajax({
            url: $("#formulario").attr('action'),
            type: 'post',
            data: $("#formulario").serialize(),
            dataType: 'text',
            success: function(data) {
                $("#senha").val(data);
                localStorage.setItem('senha', data);
            }
        });
    }

    $('.checks,select[name="tamanho"]').change(function() {
        localStorage.setItem('tamanho', $("#tamanho").val());
        recupera();
    });

    $("#formulario").submit(function(e) {
        e.preventDefault();

        if ($('#formulario input[type=checkbox]:checked').length < 1) { 
            $('input[name="minusculas"]').prop("checked", true);
        }

        recupera();
    });

    $('#minusculas').change(function() {
        if ($(this).prop("checked")) {
            localStorage.setItem('minusculas', true);       
        } else {
            localStorage.setItem('minusculas', false);       
        }
    });

    $('#maiusculas').change(function() {
        if ($(this).prop("checked")) {
            localStorage.setItem('maiusculas', true);       
        } else {
            localStorage.setItem('maiusculas', false);       
        }
    });

    $('#numeros').change(function() {
        if ($(this).prop("checked")) {
            localStorage.setItem('numeros', true);       
        } else {
            localStorage.setItem('numeros', false);       
        }
    });

    $('#simbolos').change(function() {
        if ($(this).prop("checked")) {
            localStorage.setItem('numeros', true);       
        } else {
            localStorage.setItem('numeros', false);       
        }
    });
});