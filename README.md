# Gerador de Senhas

Um gerador de senhas simples, feito em PHP e jQuery.

## Uso

`curl https://senha.lucasbrum.net`

## Demo

- [Gerador de Senhas](https://senha.lucasbrum.net)

## Créditos

- [ClipboardJS](https://clipboardjs.com/)
- Milhares de outros colaboradores...

## Ajude

Doe qualquer valor através do <a href="https://pag.ae/bfxkQW"><img src="https://img.shields.io/badge/pagseguro-green"></a> ou <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWHJL387XNW96&source=url"><img src="https://img.shields.io/badge/paypal-blue"></a>

## ScreenShot

![Screenshot][screenshot]

[screenshot]: https://gitlab.com/sistematico/gerador-de-senhas/raw/master/senha.png "ScreenShot"